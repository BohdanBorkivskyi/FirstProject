import sys
from random import randint


try:
    name = sys.argv[1]
except IndexError:
    name = "Guest_{}".format(randint(10, 99))

print("Hello, {}".format(name))
